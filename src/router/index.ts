import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/home/index.vue'),
  },
  {
    path: '/export',
    name: 'export',
    component: () => import('@/views/export/index.vue'),
  },
  {
    path: '/import',
    name: 'import',
    component: () => import('@/views/import/index.vue'),
  },
];

const router = createRouter({
  history: createWebHistory('/esPoiWeb/'),
  routes,
  scrollBehavior() {
    return {
      el: '#app',
      top: 0,
      behavior: 'smooth',
    };
  },
});

export default router;
